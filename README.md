### Game Jam 1
This project will start after cleaning up the current framework used in
a group project in IMT3601. The challenge will be according to the 
Weekly Game Jam number 172 - theme "Catapult". 

I will convert various GameObjects AI controlled catapult fish throwers. 
I will
use Finite State Machine and perhaps other AI strategies to 
automove the throwing agents. 

Project starts on 27th October at 13.00 and runs until 29th of October 13.00.


Ended project 29th of October 13.12.

### Short report
I did not even get to start with the finite state machines. I did experiment with different
ways of moving objects, using bird-flying and drone-flying consepts. I got a drone working.
I made AI paths for GameObjects to follow. The path-nodes can also be used for shooting
out objects. I demonstrate this with currently visible nodes in the sky throwing down
fish. The navigating nodes are placed on the ground having fish and an AI-penguin following.
These nodes was made before the project. 

AI navigation experiment:

* Different types of autopiloting GameObjects
* Getting the navigation from the Hummingbird-project available for general use
* Tried out bird-movement and drone-type of movements to prepare for ML-Agent reinforcement learning

Physics experiment - throwing/catapulting:

* Studied the Unity Catapult project
* Implemented a Human-Catapult (without animation)
* Different ways of throwing objects getting them in a "natural looking" direction

