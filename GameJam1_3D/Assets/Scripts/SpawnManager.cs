﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public int spawnAreaNumber;         // Spawn area 1, 2, or 3
    private float xzPosition;           // Spawn area center position
    private float spawnAreaWidthLength;

    public GameObject machinePrefab;
    private GameObject terrainObject;
    private Terrain terrain;

    private float spawnStartDelay = 10;
    private float spawnInterval = 90;

    // Start is called before the first frame update
    void Start()
    {
        terrainObject = GameObject.Find("Terrain");
        terrain = terrainObject.GetComponent<Terrain>();

        // Call SpawnMachines function after a delay, at a set interval
        InvokeRepeating("SpawnMachines", spawnStartDelay, spawnInterval);
    }

    // Update is called once per frame
    void Update()
    {
        // Calculate spawn area position and size relative to current terrain size
        float terrainWidthLength = terrain.terrainData.size.x;
        xzPosition = (terrainWidthLength / 4) * 0.8f;
        spawnAreaWidthLength = xzPosition * 1.5f;

        // Spawn areas 1, 2, and 3 have different positions
        switch (spawnAreaNumber)
        {
            case 1:
                transform.position = new Vector3(-xzPosition, 40, -xzPosition);
                break;
            case 2:
                transform.position = new Vector3(xzPosition, 40, -xzPosition);
                break;
            case 3:
                transform.position = new Vector3(xzPosition, 40, xzPosition);
                break;
        }
    }

    // Visualize spawn areas in the scene
    // Areas are represented by blue cubes
    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawCube(transform.position, new Vector3(spawnAreaWidthLength, xzPosition * 0.375f, spawnAreaWidthLength));
    }

    // Spawns one machine in random positions in each spawn area
    // Called on a set interval
    private void SpawnMachines()
    {
        // Calculate minimum and maximum X and Z positions from the spawn area center
        float maxSpawnDistanceFromAreaCenter = spawnAreaWidthLength / 2;
        float maxXRange = transform.position.x + maxSpawnDistanceFromAreaCenter;
        float minXRange = transform.position.x - maxSpawnDistanceFromAreaCenter;
        float maxZRange = transform.position.z + maxSpawnDistanceFromAreaCenter;
        float minZRange = transform.position.z - maxSpawnDistanceFromAreaCenter;

        // Find random X and Z spawn positions
        float spawnPosX = Random.Range(minXRange, maxXRange);
        float spawnPosZ = Random.Range(minZRange, maxZRange);

        Vector3 spawnPosition;   // Final calculated spawn position

        // Find Y position of machines using Raycast so they spawn on top of the terrain
        RaycastHit hit;
        float raycastDistance = 200;
        Vector3 raycastPosition = new Vector3(spawnPosX, 200, spawnPosZ);
        
        // Casts a ray downwards. The hit parameter saves the ray collision point,
        // which is the machine's spawn point
        if (Physics.Raycast(raycastPosition, Vector3.down, out hit, raycastDistance))
        {
            spawnPosition = hit.point;
        } 
        else
        {
            // If no collision, the machines spawn in midair
            spawnPosition = raycastPosition;  
        }

        // Instantiate the machine prefab at the calculated position
        Instantiate(machinePrefab, spawnPosition, machinePrefab.transform.rotation);
    }
}
