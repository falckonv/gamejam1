﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineController : MonoBehaviour
{
    // Minimum possible Y position for the machines
    private float yLimit = 2;            

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Destroy machine if it is out of bounds
        if (transform.position.y < yLimit)
        {
            Destroy(gameObject);
        }
    }
}
