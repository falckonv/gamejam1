﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IcebergShrink : MonoBehaviour
{
    private Terrain terrain;

    private float startingWidthLength = 500;  
    private float startingHeight;
    private float widthLength;
    private float height;
    private float xzPos;                   
    public float shrinkSpeed;
    private float timePassed;
    private float shrinkOverTime;

    // Start is called before the first frame update
    void Start()
    {
        timePassed = 0;
        terrain = GetComponent<Terrain>();

        // Calculate default terrain position and size so that 
        // position (0, 0, 0) is in the center of the terrain
        startingHeight = startingWidthLength * 1.2f;

        widthLength = startingWidthLength;
        height = startingHeight;
        xzPos = widthLength / 2;

        terrain.terrainData.size = new Vector3(widthLength, height, widthLength);
        transform.position = new Vector3(-xzPos, 0, -xzPos);
    }

    // Update is called once per frame
    void Update()
    {
        // Increase shrink value over time
        timePassed += Time.deltaTime;
        shrinkOverTime = timePassed * shrinkSpeed;

        // Calculate new size dimensions and position so point (0, 0, 0) 
        // is still in the center of the terrain
        widthLength = startingWidthLength - shrinkOverTime;
        height = startingHeight - (shrinkOverTime * 1.2f);
        xzPos = widthLength / 2;

        // Update terrain size and position
        terrain.terrainData.size = new Vector3(widthLength, height, widthLength);
        transform.position = new Vector3(-xzPos, 0, -xzPos);
    }
}
