﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubSimpleController : MonoBehaviour
{

    public float speed = 10.0f;
    public float rotationSpeed = 50.0f;
    public float keepHeight = 2f;  // Not yet used, can control height

    void Start()
    {
    }
    // Update is called once per frame
    void Update()
    {
        // Prepare movement changes
        float translation = Input.GetAxis("Vertical") * speed;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed;
        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;

        // TODO: if not using gravity, control the height  
         
        // Universal movments:
        //transform.Translate(0, 0, translation);
        //transform.Rotate(0, rotation, 0);
        
        // The sub (a tilted cylinder) has main movement in y-direction:
        transform.Translate(0, translation, 0);
        transform.Rotate(rotation,0,0);
    }
}
