﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishController : MonoBehaviour
{
    // Placeholder for the game objects "target" (to follow)
    public GameObject target;
    public float speed = 10.0f;
    public float rotationSpeed = 100.0f;
    public bool autoPilot = true;


    // Calculates the distance between object and target
    float calculateDistance()
    {
        Vector3 fish = this.transform.position;
        Vector3 t = target.transform.position;
        //float distance = Mathf.Sqrt(Mathf.Pow(fish.x - t.x, 2.0f) + Mathf.Pow(fish.y - t.y, 2.0f));
        float distanceUnity = Vector3.Distance(this.transform.position, target.transform.position);

        // Visualy check if distance calulated is the same as Unity function Distance
        //Debug.Log("Distance: " + distance);
        //Debug.Log("Distance Unity 3D: " + distanceUnity);
        return distanceUnity;
    }

    // Update is called once per frame
    void LateUpdate()
    {   
        if(this && target) {
            // Stop movement when close to the target
            if(calculateDistance() > 10f) 
            {
                Vector3 lookAtTarget = new Vector3(target.transform.position.x, this.transform.position.y, target.transform.position.z);
                Vector3 direction = lookAtTarget - this.transform.position;
                this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime*rotationSpeed);
                this.transform.Translate(0, 0, speed * Time.deltaTime);
            }
        }
    }
}
