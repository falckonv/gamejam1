﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Using fish objects as "bullets" for any agent that "shoot fish"
/// </summary>

public class ShootFish : MonoBehaviour
{
    public GameObject fishPrefab;       // The fish to be shot
    public GameObject spawnArea;        // The area for creating and launching the fish bullet
    public GameObject target;           // Usually a human
    public GameObject parent;           // Usually a penguine
    public float fireingRate = 0.6f;    // How often a fish can be thrown
    public float speed = 150.0f;        // Bullet speed
    public float rotationSpeed = 2.0f;  // Bullets rotation speed
    private bool  canFire = true;       // Used for timing auto-fireing
    private bool  low     = true;       // Always choose the high angle for catapult effect

    void CanFireAgain()
    {
        canFire = true;
    }

    void Fire()
    {
        if(canFire)
        {
            //Debug.Log("Can fire!");
            RotatePipe();
            GameObject fish = Instantiate(fishPrefab, spawnArea.transform.position, spawnArea.transform.rotation);
            fish.GetComponent<Rigidbody>().velocity = speed * this.transform.forward;
            canFire = false;
            Invoke("CanFireAgain", fireingRate); 
        }
    }

    float? RotatePipe()
    {
        float? angle = CalculateAngle(low);
        if (angle != null)
        {
            this.transform.localEulerAngles = new Vector3(360.0f - (float)angle, 0f, 0f);
        }
        return angle;
    }

    float? CalculateAngle(bool low)
    {
        low = false;
        Vector3 targetDirection = target.transform.position - this.transform.position;
        float y = targetDirection.y;
        float x = targetDirection.magnitude;
        float gravity = 9.81f;
        float squareSpeed = speed * speed;
        float underTheSquareRoot = (squareSpeed * squareSpeed) - gravity * (gravity * x * x + 2 * y * squareSpeed);
        
        // Only calculate if underTheSquareRoot is positive or zero
        if (underTheSquareRoot >= 0f)
        {
           float root = Mathf.Sqrt(underTheSquareRoot);
           float highAngle = squareSpeed + root;
           float lowAngle  = squareSpeed - root;

           if (low) return (Mathf.Atan2(lowAngle, gravity * x) * Mathf.Rad2Deg);
           else return (Mathf.Atan2(highAngle, gravity * x) * Mathf.Rad2Deg);
        }
        else
           return null;
    }
 
  
    // Update is called once per frame
    void LateUpdate()
    {

        // Use parent instead of "this" if stearing another object than the cube fireing fish
        // Parent can be set to "this" GameObject

        if (canFire) Fire();
        Vector3 direction = (target.transform.position - parent.transform.position).normalized;

        Debug.DrawRay(transform.position, direction*500, Color.green);

        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        parent.transform.rotation = Quaternion.Slerp(parent.transform.rotation, lookRotation, Time.deltaTime * rotationSpeed);
    }
}
