﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Code fetched from the Unity Catapult Tutorial program
public class CatapultController : MonoBehaviour
{

    public GameObject cannonBallPrefab;  // In this game the cannonBall is a fish
    public GameObject catapultArm;
    public Transform launchVector;
    public Transform springVector;
    public Transform resultVector;
    public GameObject rope;

    // Set an interval for fiering the "catapulting" GameObject
    public float fireingRate = 2;
    public float changeSpringK;

    public const float LAUNCH_SPEED_LESSON = 0.5f;
    public const float LAUNCH_SPEED_FREEPLAY = 5f;
    public float DEFAULT_LAUNCH_ANGLE = 45;
    [SerializeField] private const float cannonBallWeight = 1f;
    public float launchSpeed = 0.5f;

    public float currentArmAngle = 0f;
    public float launchAngle;

    public float ArmAngleRadians
    {
        get
        {
            return currentArmAngle * Mathf.Deg2Rad;
        }
    }

    private Quaternion armInitRotation;
    private Rigidbody cannonBall;
    public bool throwCalled = false;
    public bool launched = false;
    private bool canFire = false;

    [Header("Internal References")]
    public Transform cannonBallPos;

    // From GameManager:
    public const float DEFAULT_CANNON_BALL_MASS = 30;
    public const float DEFAULT_SPRING_FORCE = 600;
    public const float FORCE_SCALE_FACTOR = 1f; // Extra scaling needed to make the resultant force to shoot the ball
    public float DistanceFromGround_At_TimeOFLaunch;     // The minimum distance between the cannon ball and the ground to initiate the SlopeDown learning step


    public void Start()
    {
        Debug.Log("Catapult entered Start");

        // Same as in Reset() - which is not called in this code
        launched = throwCalled = false;
        launchAngle = DEFAULT_LAUNCH_ANGLE;
        rope.SetActive(true);
    }
    private void Awake()
    {
        Debug.Log("Catapult entered Awake");


        CanFire();
    }

    public void Reset()
    {
        Debug.Log("Catapult entered Reset");
        launched = throwCalled = false;
        launchAngle = DEFAULT_LAUNCH_ANGLE;
        rope.SetActive(true);
        currentArmAngle = 0;
        cannonBall.constraints = RigidbodyConstraints.FreezeAll;
        cannonBall.transform.parent = catapultArm.transform;
        catapultArm.transform.rotation = armInitRotation;
        cannonBall.transform.position = cannonBallPos.position;
    }

    private void CanFire()
    {
        rope.SetActive(true);
        //currentArmAngle = 0;
        canFire = true;
    }

    public void ThrowBall()
    {
        Debug.Log("Catapult entered ThrowBall");
        launched = true;

        GameObject ball = Instantiate(cannonBallPrefab, catapultArm.transform.position, catapultArm.transform.rotation);
        cannonBall = ball.GetComponent<Rigidbody>();

        if (changeSpringK > 0)
        {
            UpdateSpringForce(changeSpringK);
        }

        cannonBall.mass = cannonBallWeight;
        armInitRotation = catapultArm.transform.rotation;

        float velocity = Velocity_At_Time_Of_Launch();

        cannonBall.transform.SetParent(null);
        cannonBall.constraints = RigidbodyConstraints.None;
        cannonBall.useGravity = true;
        cannonBallPos = cannonBall.transform;
        cannonBall.AddForce(launchVector.up * (velocity * cannonBall.mass), ForceMode.Impulse);
        
        launched = false;
        canFire  = false;

        Invoke("CanFire", fireingRate);
    }

    private void LateUpdate()
    {
        //Debug.Log("Catapult entered LateUpdate");

        if (canFire)
        {

            Debug.Log("Catapult entered Awake and got throwCalled=true");

            ThrowBall();

             if (currentArmAngle >= launchAngle)
             {
                throwCalled = false;
                cannonBall.transform.rotation = Quaternion.identity;
                return;
             }

            currentArmAngle += (Time.deltaTime * DEFAULT_LAUNCH_ANGLE) * launchSpeed;
            catapultArm.transform.Rotate(-Vector3.up, (Time.deltaTime * DEFAULT_LAUNCH_ANGLE) * launchSpeed );

            cannonBall.transform.rotation = Quaternion.identity;
        }
    }



   #region Conditionals
    [SerializeField] private Transform tensionPoint;
    [SerializeField] private Transform springPoint;
    [SerializeField] private Transform weightPoint;
    [SerializeField] private Transform resultPoint;

    public LayerMask terrainLayer;
    

    [SerializeField] private float springK;
    [SerializeField] private float springForce;



 // Find the instantaneous velocity at the time of the cannonball's launch from the Catapault Arm
    // Formula: √(springK / m) * angle² - (g * √2)
    public float VelocityOnLaunch()
    {
        float velocity = Mathf.Sqrt(((springK / cannonBall.mass) * Mathf.Pow((this.DEFAULT_LAUNCH_ANGLE * Mathf.Deg2Rad), 2)) - (Physics.gravity.y * Mathf.Sqrt(2f)));

        return velocity;
    }

    public float SpringK
    {
        get
        {
            return springK;
        }
        set
        {
            springK = value;
            Vector3 springVector = CalculateSVector();
            SpringForce = springVector.magnitude;
        }
    }

    public float SpringForce
    {
        get
        {
            return springForce;
        }
        set
        {
            springForce = value;
            //ShowSpringChange();
        }
    }

        public void UpdateSpringForce(float _springK)
    {
        if (!springK.Equals(_springK))
        {
            SpringK = _springK;
        }
    }

    [SerializeField] private float tensionForce;
    public float TensionForce
    {
        get
        {
            return tensionForce;
        }
        set
        {
            tensionForce = value;
        }
    }

    [SerializeField] private float resultForce;
    public float ResultForce
    {
        get
        {
            return resultForce;
        }
        set
        {
            resultForce = value;
        }
    }

    #endregion

    #region Physics Formulas

    // The Elastic Potential Energy of the Cannonball as a result of the recoiled Catapult Arm 
    // Formula: ( 0.5 * springK * angle² )
    private float Delta_Elastic_Potential_Energy()
    {
        return 0.5f * SpringK * Mathf.Pow(this.DEFAULT_LAUNCH_ANGLE * Mathf.Deg2Rad, 2);
    }

    // The difference of Gravitational Potential Energy 
    // Formula: ( m * g * √2/2)
    private float Delta_Gravitational_Potential_Energy()
    {
        return Mathf.Abs(Mathf.Round(cannonBall.mass * Physics.gravity.y)) * (Mathf.Sqrt(2) / 2f);
    }

    // The ratio of (Delta Elastic Potential Energy) over (Delta Gravitation Potential Energy)
    // Formula: DEPE / DGPE
    private float ratio_Of_DEPE_Over_DGPE()
    {
        float ratio = Delta_Elastic_Potential_Energy() / Delta_Gravitational_Potential_Energy();
        return ratio;
    }

    // Equation for Kinetic Energy at time of launch 
    // Formula: (0.5 * m * vI²)
    private float Kinetic_Energy_At_Launch()
    {
        float vel = Velocity_At_Time_Of_Launch();
        return 0.5f * cannonBall.mass * Mathf.Pow(vel, 2);
    }

    // Find the instantaneous velocity at the time of the cannonball's launch from the Catapault Arm
    // Formula: √(springK / m) * angle² - (g * √2)
    public float Velocity_At_Time_Of_Launch()
    {
        float velocity = Mathf.Sqrt(((springK / cannonBall.mass) * Mathf.Pow((this.DEFAULT_LAUNCH_ANGLE * Mathf.Deg2Rad), 2)) - (Physics.gravity.y * Mathf.Sqrt(2f)));

        return velocity;
    }

    // Delta Time is equal to the Delta Vertical Velocity divided by the vertical acceleration (gravity)
    // Formula: ((vVertF - vVertI) / g)
    private float CalculateDeltaTime()
    {
        float velocity = Velocity_At_Time_Of_Launch();
        float vertVelocity = velocity * Mathf.Cos(this.DEFAULT_LAUNCH_ANGLE * Mathf.Deg2Rad);
        float vI = vertVelocity;
        float vF = -vertVelocity;
        float deltaV = vF - vI;
        float deltaTime = deltaV / Physics.gravity.y;
        //uiController.physicsUIPanel.timeText.text = Math.Round(deltaTime, 2).ToString() + " s";
        return deltaTime;
    }

    /// <summary>
    /// Distance is calculated using the horizontal component of velocity multiplies by delta time 
    /// Formula: ( vIh * dt )
    /// </summary>
    public void CalculateDistance()
    {
        float horizVelocity = Velocity_At_Time_Of_Launch() * Mathf.Sin(this.DEFAULT_LAUNCH_ANGLE * Mathf.Deg2Rad);
        float deltaTime = CalculateDeltaTime();
        float horizontalDistance = horizVelocity * deltaTime;

        // Stretch the distance gizmo to show the pre-calculated horizontal distance
        //distanceGizmo.StretchGizmo(horizontalDistance);

        // Update the Distance Text on the UI Physics Info (Top Left UI)  
        //uiController.physicsUIPanel.distanceText.text = Math.Round(horizontalDistance, 2).ToString() + " m";
    }

    // Force that is the combined Normal and Centrifugal force of the catapult spoon on the cannonball as the arm rises
    // Formula: (Nx+Cx, Ny+Cy)
    private Vector3 CalculateSVector()
    {
        return (NormalVector() + CentrifugalVector()) * new Vector3(-1, 1, 0); // Multiply x by -1 to horizontally flip the S Vector to the catapult's local orientation
    }

    // Spring Force vector that is perpendicular to the catapault arm (Force that acts on the cannonball from the catapult spoon as the arm rises)
    private Vector2 NormalVector()
    {
        float angle = this.ArmAngleRadians;
        Vector2 normalizedNormal = new Vector2(0, 1);   // Perpendicular 90 degrees to catapault arm

        return normalizedNormal * ratio_Of_DEPE_Over_DGPE() * Mathf.Abs(Mathf.Round(cannonBall.mass * Physics.gravity.y)) * (1 - (2 * angle) / Mathf.PI);
    }

    // Centrifugal Force vector that is Parallel to Catapault arm (Force that keeps the cannonball horizontally in the spoon as the arm rises)
    private Vector2 CentrifugalVector()
    {
        float angle = this.ArmAngleRadians;
        Vector2 normalizedCentrifugal = new Vector2(1, 0);  // Parallel 0 degrees to catapault arm

        return normalizedCentrifugal * (ratio_Of_DEPE_Over_DGPE() * Mathf.Abs(Mathf.Round(cannonBall.mass * Physics.gravity.y)) * ((2 * angle) / Mathf.PI));
    }

    #endregion

}
