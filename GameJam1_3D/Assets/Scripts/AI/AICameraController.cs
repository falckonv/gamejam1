﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

// This is for pushing a camera on the MAIN AIplayer (should only be ONE)

public class AICameraController : MonoBehaviour
{
    public GameObject AIplayer;  // Choose the GameObject to be followed by camera
    public float speed;
    public float rotationSpeed = 100;
    private float horizontalArrowKeyInput;
    private float verticalArrowKeyInput;
    private Vector3 eulerAngleVelocity;
     // Minimum and maximum camera pitch
    private float minXAngle = -40;
    private float maxXAngle = 15;
    private Vector3 offset = new Vector3(0.41f, 3.86f, -7.57f);

    // Start is called before the first frame update
    void Start()
    {
        eulerAngleVelocity = new Vector3(0, rotationSpeed, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (AIplayer) {
            // Camera follows player in AI testing (can be any GameObject playable or AI)
            transform.position = AIplayer.transform.position + offset;
            //Debug.Log("AIplayer is registered");

            // Yaw the camera with the left and right arrow keys
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                horizontalArrowKeyInput = -1;
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                horizontalArrowKeyInput = 1;
            }
            else
            {
                horizontalArrowKeyInput = 0;
            }

            // Pitch the camera with the up and down arrow keys
            if (Input.GetKey(KeyCode.UpArrow))
            {
                verticalArrowKeyInput = -1;
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                verticalArrowKeyInput = 1;
            }
            else
            {
                verticalArrowKeyInput = 0;
            }

            // Rotate camera around the focal point's local X axis to pitch
            // Rotate camera around the focal point's global Y axis to yaw
            transform.Rotate(new Vector3(verticalArrowKeyInput * rotationSpeed * Time.deltaTime, 0, 0));
            transform.Rotate(new Vector3(0, horizontalArrowKeyInput * rotationSpeed * Time.deltaTime, 0), Space.World);

            // Restrict camera pitch, gotten from
            // https://answers.unity.com/questions/1382504/mathfclamp-negative-rotation-for-the-10th-million.html
            transform.localEulerAngles = 
                new Vector3(
                    Mathf.Clamp((transform.localEulerAngles.x <= 180) ? 
                    transform.localEulerAngles.x : 
                    -(360 - transform.localEulerAngles.x), minXAngle, maxXAngle), 
                    transform.localEulerAngles.y, 
                    transform.localEulerAngles.z); 
        }
        
        else Debug.Log("AIplayer not recognized by AICameraController");
    } 
}
