using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// A tracker that other agents (fish) can follow
/// The trackers routes has to be set up according to positioned nodes
/// in the game. This script CAN REPLACE the FishController.cs
/// Should be embedded in the GameObject that uses the tracker as its
/// source of movement!
/// Make sure the tracker has a slightly higher speed than the
/// GameObject that follws it
/// </summary>

public class Tracker : MonoBehaviour
{   
    // Way points (GameObjects) as guides for the tracker to move
    public GameObject[] waypoints;
    // CurrentIndex is the current index in the waypoints
    private int currentIndex = 0;
    private GameObject tracker;
    private GameObject target;

    public float speed = 10.0f;
    public float rotationSpeed = 100.0f;
    public float smoothing = 20.0f;
    public float lookAhead = 10.0f;
    public bool trackerOn = true;


    void Start() 
    {   
        // Make a tracker without box collision or mesh (invisible)
        tracker = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        //DestroyImmediate(tracker.GetComponent<Collider>());
        tracker.GetComponent<MeshRenderer>().enabled = false;
        // Place the tracker
        tracker.transform.position = this.transform.position;
        tracker.transform.rotation = this.transform.rotation;
    }

    void RunTracker() 
    {
        if (Vector3.Distance(tracker.transform.position, this.transform.position) > lookAhead) return;
        if (Vector3.Distance(tracker.transform.position, waypoints[currentIndex].transform.position) < 3.0f)
        {   
            // If near enough to current waypoint, choose the next
            currentIndex++;
        }
        if (currentIndex >= waypoints.Length)
        {
            // Reset index if reached the end of waypoint list
            currentIndex = 0;
        }

        // Aim tracker at the current waypoint
        tracker.transform.LookAt(waypoints[currentIndex].transform);
        tracker.transform.Translate(0.0f, 0.0f, (speed + smoothing) * Time.deltaTime);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        RunTracker();
        // Setting up to move the GameObject following the tracker
        Quaternion lookAtWaypoint = Quaternion.LookRotation(tracker.transform.position - this.transform.position);
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, lookAtWaypoint, Time.deltaTime * rotationSpeed);
        this.transform.Translate(0, 0, speed * Time.deltaTime);
    }
}
