﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The FishArea script is placing the charming fish in
/// the game environment
/// TODO: implement
/// </summary>

public class FishArea : MonoBehaviour
{
	// Diameter of the area where the agent and fish can be
	// based on the world size (20 meter is max distance)
	public const float AreaDiameter = 20f;

	// List of all fish plants in this fish area (fish plants have mulitple fishes)
	private List<GameObject> fishSteam;
	// A lookup dictionary for looking up a fish from a nectar collider
	private Dictionary<Collider, Fish> charmFishDictionary; 
	/// <summary>
	/// The list of all fishes in the fish area
	/// </summary>
	public List<Fish> Fishes { get; private set; }
	/// <summary>
	/// Reset the fishes and fish plants
	/// </summary>
	public void ResetFishes()
	{
	// Rotate all fish plat around the Y axis and subtly aroud X and Z
	foreach (GameObject fishPlant in fishSteam)
	{
		float xRotation = UnityEngine.Random.Range(-5f,5f); // random rotation around x-axis
		float yRotation = UnityEngine.Random.Range(-180f,180f); // random rotation around x-axis
		float zRotation = UnityEngine.Random.Range(-5f,5f); // random rotation around x-axis
		fishPlant.transform.localRotation = Quaternion.Euler(xRotation, yRotation, zRotation);
	}

	// Reset all fishes
	foreach (Fish fish in Fishes)
	{
		fish.ResetFish();
	}
	}

	/// <summary>
	/// Gets the <see cref="Fish"/> that a nectar collider belongs to
	/// </summary>
	/// <param name="collider">The charm collider</param>
	/// <returns>The matching fish</returns>
	public Fish GetFishFromCharm(Collider collider)
	{
		return charmFishDictionary[collider];
	}
	/// <summary>
	/// Called when the area wakes up
	/// </summary>
	private void Awake()
	{
		// Initialize variables
		fishSteam = new List<GameObject>();
		charmFishDictionary = new Dictionary<Collider, Fish>();
		Fishes = new List<Fish>();

		// Find all fishes that are children of this GameObject fish
		// Simulating a fish steam
		FindChildFishes(transform);
	}

	/// <summary>
	/// Recursively finds all fishes and fish plants that are children of a parent transform
	/// </summary>
	/// <param name="parent">The parent of the children to check</param>
	private void FindChildFishes(Transform parent)
	{
	for (int i = 0; i < parent.childCount; i++) 
	{
		Transform child = parent.GetChild(i);

	if (child.CompareTag("fish_plant"))
	{
		// Found a fish steam, add it to the fishSteam list
		fishSteam.Add(child.gameObject);

		// Look for fishes within the fish plant (should be)
		FindChildFishes(child);
	}
	else
	{
		// Not a fish steam, look for a Fish components
		Fish fish = child.GetComponent<Fish>();
		if (fish != null)
		{
		// Found a fish, add it to the Fishes list
		Fishes.Add(fish);
		// Add the charm collider to the lookup dictionary
		charmFishDictionary.Add(fish.nectarCollider, fish);
		// Note: there are no fishes that are children of other fishes
		}
		else
		{
		// Fish component not found, so check children
		FindChildFishes(child);
		}
	}
	}
	}	
}  


