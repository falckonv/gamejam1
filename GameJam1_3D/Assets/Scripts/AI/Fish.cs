﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish : MonoBehaviour
{

    [Tooltip("The color when the fish has charm")]
    public Color fullFishColor = new Color(1f, 0f, .3f);

    [Tooltip("The color when the fish have no charm left")]
    public Color emptyFishColor = new Color(.5f, 0f, 1f);

    /// <summary>
    /// The trigger collider representing the transfer of charm
    /// </summary>
    [HideInInspector]
    public Collider nectarCollider;

    // The solid collider representing the charm-collision
    private Collider fishCollider;

    // The fish's material
    private Material fishMaterial;

    /// <summary>
    /// A vector pointing straight out of the fish
    /// </summary>
    public Vector3 FishUpVector
    {
    get
    {
        return nectarCollider.transform.up;
    } 
    }

    /// <summary>
    /// The center position of the nectar collider
    /// </summary>
    public Vector3 FishCenterPosition
    {
        get
        {
            return nectarCollider.transform.position;
        }
    }

    /// <summary>
    /// The amount of charm remaining in the fish
    /// </summary>
    public float CharmAmount { get; private set; }

    /// <summary>
    /// Whether the fish has any remaining charm
    /// </summary>
    public bool HasCharm
    {
        get
        {
            return CharmAmount > 0f;
        }
    }

    /// <summary>
    /// Attempts to remove charm from the fish
    /// </summary>
    /// <param name="amount">The amount of fish to remove from global storage</param>
    /// <returns>The actual amount successfully removed</returns>
    public float Feed(float amount)
    {
        // Track how much charm that was successfully collected (cannot take more than what is left)
        float nectarTaken = Mathf.Clamp(amount, 0f, CharmAmount);
        // Subtract the charm
        CharmAmount -= amount;

        if (CharmAmount <= 0)
        {
        // No charm remaining
        CharmAmount = 0;

        // Disable the fish and charm colliders
        fishCollider.gameObject.SetActive(false);
        nectarCollider.gameObject.SetActive(false);

        // Change the fish color (using a shader)
        fishMaterial.SetColor("_BaseColor", emptyFishColor);
        }

        return nectarTaken;
    }

    /// <summary>
    /// Resets the fish
    /// </summary>
    public void ResetFish()
    {
        // Refill the charm at reset
        CharmAmount = 1f;
        // Enable the fish and charm colliders
        fishCollider.gameObject.SetActive(true);
        nectarCollider.gameObject.SetActive(true);
        // Change the fish color to indicate that it is full of charm
        fishMaterial.SetColor("_BaseColor", fullFishColor);
    }

    /// <summary>
    /// Called when the fish wakes up
    /// </summary>
    private void Awake()
    {
        // MESH renderer on the fish - has a material
        // Find the fish's mesh renderer and get the main material
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        fishMaterial = meshRenderer.material;
        
        // Find fish and charm colliders
        fishCollider = transform.Find("FishCollider").GetComponent<Collider>();
        nectarCollider = transform.Find("FishCharnCollider").GetComponent<Collider>();
    }
}
