﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubmarineController : MonoBehaviour
{

    private Rigidbody rigidbody;
    private Transform selfTransform;


    [Tooltip("Force to apply when moving")]
	public float moveForce = 5f;

	[Tooltip("Speed to pitch up or down")]
	public float pitchSpeed = 100f;

	[Tooltip("Speed to rotate around the up axis")]
	public float yawSpeed = 100f;

	[Tooltip("Whether this is training mode or gameplay")]
	public bool trainingMode;

	// Allows for smoother pitch change (necessary for neuralnetwork stearing smooth)
	private float smoothPitchChange = 0.3f;
	private float smoothYawChange = 0.3f;

	// Maximum angle that the bird can pitch up or down set to 80 degrees
	private const float MaxPitchAngle = 80f;

    // Placeholder for movements in five 3D directions (simulates flying)
    private float[] vectorAction;
    public float throttleSpeed;
    float pitch = 0f;
    float yaw = 0f;



    // Start is called before the first frame update
    void Start()
    {
        selfTransform = transform;
        rigidbody = GetComponent<Rigidbody>(); // Is the gameobject itself
    }
    
    // Get the players key inputs and transfer them to changes in the five 3D directions
    private bool GetKeys()
    {
       

        Vector3 forward = Vector3.zero;  // Set default to not moving
        Vector3 left = Vector3.zero;
        Vector3 up = Vector3.zero;
        pitch = 0f;
        yaw   = 0f;

        bool keypressed = false;

        // Convert keyboard inputs to movement and turning all values between -1 and +1

        // Forward/backward
        if (Input.GetKey(KeyCode.W))
        {
             forward = transform.forward;
             Debug.Log("Key W pressed");
             keypressed = true;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            forward = -transform.forward;
            Debug.Log("Key S pressed");
            keypressed = true;
        }

        // Left/right
        if (Input.GetKey(KeyCode.A))
        {
            left = -transform.right;
            Debug.Log("Key A pressed");
            keypressed = true;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            left = transform.right;
            Debug.Log("Key D pressed");
            keypressed = true;
        }

        // Forward/backward
        if (Input.GetKey(KeyCode.E))
        {
            up = transform.up;
            Debug.Log("Key E pressed");
            keypressed = true;
        }
        else if (Input.GetKey(KeyCode.C))
        {
            up = -transform.up;
            Debug.Log("Key D pressed");
            keypressed = true;
        }

        // Pitch up/down
        if (Input.GetKey(KeyCode.D))
        {
            pitch = 1f;
            Debug.Log("Key D pressed");
            keypressed = true;
        }
        else if (Input.GetKey(KeyCode.V))
        {
            pitch = -1f;
            Debug.Log("Key V pressed");
            keypressed = true;
        }

        // Turn left/right
        if (Input.GetKey(KeyCode.J))
        {
            yaw = -1f;
            Debug.Log("Key J pressed");
            keypressed = true;
        }
        else if (Input.GetKey(KeyCode.L))
        {
            yaw = 1f;
            Debug.Log("Key L pressed");
            keypressed = true;
        }

        // Stop
        if (Input.GetKey(KeyCode.Q))  // Spacebar
        {
            forward = new Vector3(0,0,0);
            left = new Vector3(0,0,0);
            up = new Vector3(0,0,0);
            pitch = 0f;
            yaw = 0f;
            keypressed = true;
        }

        // Combine the movement vectors and normalize
        Vector3 combined = (forward + left + up).normalized;

        // Forwards the complete 3D movements
        vectorAction[0] = combined.x;
        vectorAction[1] = combined.y;
        vectorAction[2] = combined.z;
        vectorAction[3] = pitch;
        vectorAction[4] = yaw;

        return keypressed;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        vectorAction = new float[5];

        if(GetKeys()) 
        {

        // Calculate movement vector
        Vector3 move = new Vector3(vectorAction[0], vectorAction[1], vectorAction[2]);

        // Add force in the direction of the move vector (gives a cerain slowness when
        // changing directions = more natural)
        rigidbody.AddForce(move * moveForce);
        

        // Make rotation
        Vector3 rotationVector = transform.rotation.eulerAngles;

        // Smoothing pitch and yaw
        smoothPitchChange = Mathf.MoveTowards(smoothPitchChange, vectorAction[3], 2f * Time.fixedDeltaTime);
        smoothYawChange   = Mathf.MoveTowards(smoothYawChange,   vectorAction[4], 2f * Time.fixedDeltaTime);

        // Calculate new pitch and yaw based on smoothed values
        // Clamp pitch to avoid flipping upside down
        float pitch = rotationVector.x + smoothPitchChange * Time.fixedDeltaTime * pitchSpeed;
        if (pitch > 180f) {
            pitch -= 360f;
        }
        
        // Restrict pitch to max level rise angle
        pitch = Mathf.Clamp(pitch, -MaxPitchAngle, MaxPitchAngle);
        
        // Do not restrict yaw as the circular movement left or right is ok
        float yaw = rotationVector.y + smoothYawChange * Time.fixedDeltaTime * yawSpeed;
        

        transform.rotation = Quaternion.Euler(pitch, yaw, 0f);
        }

        }
}

