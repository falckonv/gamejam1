﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;  // NavMesh if used


/// <summary>
/// FSM - final state machine for 
/// AI in penguines-game
/// </summary>
public class FSM
{
    public enum STATE
    {
        IDLE,
        PATROL,
        ATTACK,
        PURSUE,
        EVADE,
        HIDE,
        EAT
    };

    public enum EVENT
    {
        ENTER, 
        UPDATE, 
        EXIT
    };

    public    STATE name;                     // Name of the STATE
    protected EVENT stage;                    // Store the stage the EVENT is in
    protected FSM nextState;                  // Store the next state as FSM object

    public float visibleDistance  = 10.0f;
    public float visibleAngle     = 30.0f;    // Is 2 * 30 = 60 degrees
    public float shootingDistance = 30.0f;

    protected Animator animator;               // The agents animator
    protected Transform tagent;                // The agents transform
    protected NavMeshAgent agent;              // The agent (navmesh)
    //protected GameObject[] npc;              // Can use array if more than one "target"
    protected GameObject npc;                  // Use one GameObject as target (use other methods for more than one)
    

    // Constructor for FSM
    public FSM (GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _tagent)
    {
        npc        = _npc;            // Is a list of nodes for the agent to "visit"
        animator   = _anim;           // Animation
        agent      = _agent;          // If NavMesh is used, this should be the NavMeshAgent
        stage      =  EVENT.ENTER;    // Start the event
        tagent     = _tagent;         // Penguine (apply transfrom)
    }

    public virtual void Enter()
    {
        stage = EVENT.UPDATE;
    }

    public virtual void Update()
    {
        stage = EVENT.UPDATE;
    }

    public virtual void Exit()
    {
        stage = EVENT.EXIT;
    }

    public virtual FSM StateProcess()
    {
        if (stage == EVENT.ENTER) Enter();
        if (stage == EVENT.UPDATE) Update();
        if (stage == EVENT.EXIT) 
        {
            Exit();
            return nextState;
        }
        return this; // If not returning next state, return current
    }

    public bool CanSeePlayer()
    {
        Vector3 direction = tagent.position - npc.transform.position;
        float angle = Vector3.Angle(direction, npc.transform.forward);

        if (direction.magnitude < visibleDistance && angle < visibleAngle)
        {
            return true;  // npc can see the agent
        }
        return false;     // npc can not see the agent
    }
}

public class Patrol : FSM
{
    // Provide an external queue of GameObjects to "patrol" (nodes)
    // CurrentIndex is used to count through the list
    int currentIndex = -1;

    public Patrol(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _tangent)
        : base(_npc, _agent, _anim, _tangent)
        {
            name = STATE.PATROL;
            agent.speed = 2;
            agent.isStopped = false;
        }

    public override void Enter()
    {
        currentIndex = 0;
        animator.SetTrigger("isWalking");
        base.Enter();
    }

    public override void Update()
    {

        /* // If using GameObject[]
        if (agent.remainingDistance < 1)
        {
            if (currentIndex >= npc.Length - 1)
            {
                currentIndex = 0;
            }
            else currentIndex ++;

            agent.SetDestination(npc[currentIndex].transform.position);
        }
*/

        base.Update();
    }

    public override void Exit()
    {
        animator.ResetTrigger("isWalking");
        base.Exit();
    }
}

public class Idle : FSM
{
    public Idle (GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _tagent)
        : base(_npc, _agent, _anim, _tagent) 
        {
            name = STATE.IDLE;
        }

    public override void Enter()
    {
        animator.SetTrigger("isIdle");
        base.Enter();
    }

    public override void Update()
    {
        if(Random.Range(0,500) < 10) 
        {
            nextState = new Patrol(npc, agent, animator, tagent);
            stage = EVENT.EXIT;
        }
        base.Update();
    }

    public override void Exit()
    {
        animator.ResetTrigger("isIdle");
        base.Exit();
    }
}

