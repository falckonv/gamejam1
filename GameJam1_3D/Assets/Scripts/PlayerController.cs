﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody playerRb;
    private Animator playerAnim;

    public GameObject charmFishPrefab;

    // XZ movement
    private float verticalInput;
    private float horizontalInput;
    private float speed;
    private float walkSpeed = 5;
    private bool sliding;
    private float slideSpeed = 20;

    // Y movement
    private float yLimit = 3;                     // Minimum possible Y position for the player
    private float jumpForce = 130;
    public bool isOnGround;                 
    private float raycastMidairDistance = 1.2f;   // Minimum height from ground colliders to consider player midair
    private float raycastOnGroundDistance = 0.2f; // Minimum height from ground colliders to consider player to be on the ground

    // Player rotation
    private float rotationSpeed = 100;
    private Vector3 eulerAngleVelocity;

    // Combat variables
    private int attackType;
    private bool defending;
    private bool attackCooldown;  // To avoid attack spam

    // Player property variables;
    private float maxHealthPoints = 100;
    public float healthPoints;
    public bool hasSpeedPowerup;
    private float speedPowerupMultiplier;
    public bool charmFishEquipped;

    // Start is called before the first frame update
    void Start()
    {
        playerRb = GetComponent<Rigidbody>();
        playerAnim = GetComponent<Animator>();

        isOnGround = true;
        defending = false;
        attackCooldown = false;

        healthPoints = 50;
        hasSpeedPowerup = false;
        charmFishEquipped = false;

        // Player rotation speed vector
        eulerAngleVelocity = new Vector3(0, rotationSpeed, 0);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Game over if player is out of bounds
        if (transform.position.y < yLimit)
        {
            //transform.position = new Vector3(82.5f, 40, -147);
            Debug.Log("Game Over");
        }

        AttackControls();
        DefendControls();
        JumpAndMidairControls();
        XZMovementControls();
    }

    private void AttackControls()
    {
        // Start attack animation when Enter is pressed, when the attack is not on cooldown
        if (!sliding && Input.GetKeyDown(KeyCode.Return) && !attackCooldown)
        {
            // Alternate randomly between attack animations 1 and 2
            attackType = Random.Range(1, 3);
            playerAnim.SetInteger("Attack type", attackType);
            playerAnim.SetTrigger("Attack");

            // Starts cooldown and stops cooldown after a time
            attackCooldown = true;
            Invoke("ResetCooldown", 0.5f);
        }

        // Throw charm fish if the fish is equipped by pressing the Right Control key,
        // if not transitioning to sliding, sliding, or transitioning to standing position
        bool standToSlideAnimation = playerAnim.GetCurrentAnimatorStateInfo(0).IsName("StandToSlide");
        bool slidingAnimation = playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Slide");
        bool slideToStandAnimation = playerAnim.GetCurrentAnimatorStateInfo(0).IsName("SlideToStand");

        if (!standToSlideAnimation && !slidingAnimation && !slideToStandAnimation && !sliding &&
            Input.GetKeyDown(KeyCode.RightControl) && charmFishEquipped)
        {
            playerAnim.SetInteger("Attack type", 1);
            playerAnim.SetTrigger("Attack");
            charmFishEquipped = false;
            Vector3 charmFishThrowStartPos = 
                transform.position + (transform.forward * 2) + (transform.right * 1.2f) + new Vector3(0, 2, 0);
            Instantiate(charmFishPrefab, charmFishThrowStartPos, transform.rotation);
        }
    }

    private void ResetCooldown()
    {
        attackCooldown = false;
    }

    private void DefendControls()
    {
        // Player defends when holding Right Shift
        if (Input.GetKey(KeyCode.RightShift) && !sliding)
        {
            playerAnim.SetBool("Defending", true);
            defending = true;
        }
        else
        {
            playerAnim.SetBool("Defending", false);
            defending = false;
        }
    }

    private void JumpAndMidairControls()
    {
        // If the player is far enough from colliders in the negative Y direction,
        // MidAir animation plays
        if (!Physics.Raycast(transform.position, Vector3.down, raycastMidairDistance))
        {
            playerAnim.SetBool("Midair", true);
        }
        else
        {
            playerAnim.SetBool("Midair", false);
        }

        // Checks if player is standing on the ground
        if (!Physics.Raycast(transform.position, Vector3.down, raycastOnGroundDistance))
        {
            isOnGround = false;
        }
        else
        {
            isOnGround = true;
        }

        // Jump with space bar, only possible when starting from the ground and when not defending
        if (Input.GetKeyDown(KeyCode.Space) && isOnGround && !defending)
        {
            playerRb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);

            // Start jump animation if not sliding
            if (!sliding)
            {
                playerAnim.SetTrigger("Jump");
            }
        }
    }

    private void XZMovementControls()
    {
        // W and S keys control forward and backward movement
        verticalInput = Input.GetAxisRaw("Vertical");

        // Player is sliding if moving forward while Left Shift is held down
        sliding = Input.GetKey(KeyCode.LeftShift) && verticalInput > 0;

        // Change movement speed depending on whether the player is sliding
        switch (sliding)
        {
            case true: speed = slideSpeed; break;
            case false: speed = walkSpeed; break;
        }

        // Double the speed when the player is powered up
        switch(hasSpeedPowerup)
        {
            case true: speedPowerupMultiplier = 2; break;
            case false: speedPowerupMultiplier = 1; break;
        }

        // Move player Rigidbody if not in defending position
        if (!defending)
        {
            Vector3 movement = transform.forward * verticalInput * speed * speedPowerupMultiplier * Time.deltaTime;
            playerRb.MovePosition(transform.position + movement);
        }

        // Control animations for when W (forward) or S (backward) is held,
        // and when standing still
        if (verticalInput > 0)
        {
            playerAnim.SetBool("Moving", true);
            if (sliding)
            {
                playerAnim.SetBool("Sliding", true);
            }
            else
            {
                playerAnim.SetBool("Sliding", false);
            }
        }
        else if (verticalInput < 0)
        {
            playerAnim.SetBool("Moving", true);
            playerAnim.SetBool("Sliding", false);
        }
        else
        {
            playerAnim.SetBool("Moving", false);
            playerAnim.SetBool("Sliding", false);
        }

        // A and D keys control player rotation
        horizontalInput = Input.GetAxisRaw("Horizontal");

        // Rotate player Rigidbody
        Quaternion deltaRotation = Quaternion.Euler(eulerAngleVelocity * horizontalInput * Time.deltaTime);
        playerRb.MoveRotation(playerRb.rotation * deltaRotation);
    }

    private void OnCollisionEnter(Collision collision)
    {
        GameObject fishCollectParticle;

        if (collision.gameObject.CompareTag("SpeedFish"))
        {
            // Play fish explosion particle effect
            fishCollectParticle = GameObject.Find("Yellow Explosion");
            fishCollectParticle.transform.position = collision.transform.position;
            fishCollectParticle.GetComponent<ParticleSystem>().Play();

            // Player now has powerup effect, which goes away after a few seconds
            Destroy(collision.gameObject);
            hasSpeedPowerup = true;
            StartCoroutine(SpeedPowerupDuration());
        } 
        else if (collision.gameObject.CompareTag("HealthFish"))
        {
            // Play fish explosion particle effect
            fishCollectParticle = GameObject.Find("Red Explosion");
            fishCollectParticle.transform.position = collision.transform.position;
            fishCollectParticle.GetComponent<ParticleSystem>().Play();

            // Heal player, ensure health points cannot be higher than the maximum health points
            Destroy(collision.gameObject);
            healthPoints += 30;
            if (healthPoints > maxHealthPoints) healthPoints = maxHealthPoints;
        }
        else if (collision.gameObject.CompareTag("CharmFish"))
        {
            // Charm fish is now equipped
            Destroy(collision.gameObject);
            charmFishEquipped = true;
        }
    }

    IEnumerator SpeedPowerupDuration()
    {
        yield return new WaitForSeconds(10);
        hasSpeedPowerup = false;
    }
}
